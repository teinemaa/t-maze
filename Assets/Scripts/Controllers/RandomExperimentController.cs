﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class RandomExperimentController : ExperimentController {

    private int populationSize = 50;
	private float mouseSpeed = 3f;
	private float timeLimitSeconds = 5f;
	private float smallReward = 0.2f;
	private float bigReward = 1f;
    private List<MouseController> mouseControllers = new List<MouseController>(); 
    public override void Init()
    {
    }

    public override bool HasNextExperiment()
    {
        return true;
    }

    public override Experiment RunNextExperiment()
    {
        for (int i = 0; i < populationSize; i++)
        {
			GameObject mouseControllerObject = ((GameObject)Instantiate(mouseGameObject, Vector3.zero, Quaternion.identity));
			var mouseController = mouseControllerObject.AddComponent<RandomContinuousController>();
			mouseController.transform.parent = transform;
            mouseControllers.Add(mouseController);
			mouseController.GetComponent<MouseProperties>().Speed = mouseSpeed;
        }
		bool bigRewardLeft = Random.value > 0.5f;
		float leftReward = bigRewardLeft ? bigReward : smallReward;
		float rightReward = bigRewardLeft ? smallReward : bigReward;
		return new Experiment(mouseControllers, leftReward, rightReward, timeLimitSeconds);
    }

    public override void EndExperiment()
    {
        foreach (MouseController mouseController in mouseControllers)
        {
            Destroy(mouseController.gameObject);
        }
        mouseControllers.Clear();
    }
}
