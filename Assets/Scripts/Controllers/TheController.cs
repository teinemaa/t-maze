﻿using SharpNeat.Phenomes;
using UnityEngine;
using System.Collections;

public class RandomContinuousController : ContinuousMouseController
{

	public const float stepDistance = 0.1f;
	// Use this for initialization
	public RandomContinuousController() : base(stepDistance)
    {
    }


    public override ContinuousOutput AdvanceContinuous(ContinuousInput input)
    {
        IBlackBox box = GetComponentInParent<MouseNeatUnit>().Box;
        box.InputSignalArray[0] = input.GetReward();
        box.InputSignalArray[1] = normalizeDistance(input.GetRangeFinderDistance(0));
        box.InputSignalArray[2] = normalizeDistance(input.GetRangeFinderDistance(-60));
        box.InputSignalArray[3] = normalizeDistance(input.GetRangeFinderDistance(60));
        box.Activate();
        float turnAngle = 40 * (float)(box.OutputSignalArray[0] - 0.5f);
        //print(box.OutputSignalArray[0]);
		return new ContinuousOutput(turnAngle);
    }

    private static float normalizeDistance(float distance)
    {
        return Mathf.Min(1f, distance);
    }

    private static float BoolToFloat(bool b)
    {
        return b ? 1f : 0f;
    }
}
