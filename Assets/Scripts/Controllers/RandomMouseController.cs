﻿using SharpNeat.Phenomes;
using UnityEngine;
using System.Collections;

public class RandomMouseController : MouseController
{
    public override MouseOutput AdvanceMouse(MouseInput input)
	{



            IBlackBox box = GetComponent<MouseNeatUnit>().Box;
        box.InputSignalArray[0] = input.GetReward();
        box.InputSignalArray[1] = normalizeDistance(input.GetRangeFinderDistance(0));
        box.InputSignalArray[2] = normalizeDistance(input.GetRangeFinderDistance(-60));
        box.InputSignalArray[3] = normalizeDistance(input.GetRangeFinderDistance(60));
        box.Activate();
        float turnAngle = 30 * (float)(box.OutputSignalArray[0] - box.OutputSignalArray[1]);
        float stepDistance = 30 * (float)(box.OutputSignalArray[2]);
        return new MouseOutput(turnAngle, stepDistance);
    }

    private static float normalizeDistance(float distance)
    {
        return distance/5;
    }

    private static float BoolToFloat(bool b)
    {
        return b ? 1f : 0f;
    }
}
