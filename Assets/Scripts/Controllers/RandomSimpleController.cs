﻿using SharpNeat.Phenomes;
using UnityEngine;
using System.Collections;

public class RandomSimpleController : SimpleMouseController
{
    public override SimpleOutput AdvanceSimple(SimpleInput input)
    {


        IBlackBox box = GetComponentInParent<MouseNeatUnit>().Box;
        box.InputSignalArray[0] = input.GetReward();
        box.InputSignalArray[1] = BoolToFloat(input.IsMazeEnd());
        box.Activate();

        SimpleOutput direction;
        if (box.OutputSignalArray[0] > box.OutputSignalArray[1])
        {
            direction   = SimpleOutput.RIGHT;
        }
        else
        {

            direction = SimpleOutput.LEFT;
        }
        return direction;
    }



    private static float BoolToFloat(bool b)
    {
        return b ? 1f : 0f;
    }


}
