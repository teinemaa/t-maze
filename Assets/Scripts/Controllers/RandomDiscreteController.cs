﻿
using UnityEngine;

public class RandomDiscreteController : DiscreteMouseController
{
    public override DiscreteOutput AdvanceDiscrete(DiscreteInput input)
    {
		DiscreteOutput direction = (DiscreteOutput)(90 * Random.Range (-1, 2));
        return direction;
    }
}
