﻿using UnityEngine;

public class SelectMouse : MonoBehaviour
{

    public bool selected = false;


    void OnMouseDown()
    {
        selected = !selected;

        renderer.enabled = selected;
    }

}

