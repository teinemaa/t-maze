﻿using UnityEngine;
using System.Collections;

public class Hud : MonoBehaviour
{

    public Sprite bigReward;
    public Sprite smallReward;
    public Sprite noReward;
    public Sprite switchSprite;

    public float width;

    private float size;
    private int populationSize;
    private int currentRewardIndex;

    // Use this for initialization
	void Awake ()
	{


	}

    public void Init(int populationSize)
    {
        this.populationSize = Mathf.Max(populationSize+1, 9);
        size = width / this.populationSize;
        currentRewardIndex = 0;
    }

    public void AddReward(float reward)
    {
        if (reward == 0f)
        {
            AddReward(noReward);
        }
        else if (reward == 1f)
        {
            AddReward(bigReward);
        }
        else
        {
            AddReward(smallReward);
        }
    }
    public void AddSwitch()
    {
        AddReward(switchSprite);
    }

    private void AddReward(Sprite reward)
    {
        GameObject rewardObject = new GameObject("reward_1");
        rewardObject.transform.parent = transform;
        rewardObject.AddComponent<SpriteRenderer>().sprite = reward;
        rewardObject.transform.localScale = new Vector3(size * 0.9f, size * 0.9f, 1);
        rewardObject.transform.position = transform.position + new Vector3(-size * (populationSize - 1) / 2 + currentRewardIndex * size, size / 2 + 0.5f, 0);
        currentRewardIndex++;
    }


}
