﻿using System.IO;
using System.Linq;
using SharpNeat.Phenomes.NeuralNets;
using UnityEngine;
using System.Collections;

public class InteractiveExperiments : MonoBehaviour
{

    public GameObject death;

    public int widthCount;
    public int heightCount;
    public int tmazeWidth;
    public int tmazeHeight;
    public GameObject tmaze;

    private GameObject mazesParent;
    private float initialTimestep;
    private int mazesFinished;
    private int totalMazes;
    private float hSliderValue = 0;
    private float speedModifier = -1f;
    private int i;
    private int j;

    public bool interactive = true;
//    private bool rewardRight;
//    private int switchIndex;

    private System.Random rand = new System.Random(0);
    private bool running = false;
    private bool firstRun = true;

    private uint finisherRoundNr = 0;

    public int runsLeft;
    private float startedTime;
    private bool start = false;
    private int startCountdown = 0;
    private StreamWriter resultFile;

    void Awake()
    {
        initialTimestep = Time.fixedDeltaTime;
    }

    void Start()
    {
        string directoryName = "results";
        Directory.CreateDirectory(directoryName);

        for (int i = 0; true; i++)
        {
            string fileName = directoryName + "/" + i+".txt";
            if (!File.Exists(fileName))
            {
                resultFile = File.CreateText(fileName);
                resultFile.WriteLine(FastCyclicNetwork._hebbianLearning + "," + FastCyclicNetwork._neuromodulatedPlasticity + "," + FastCyclicNetwork._l + "," + FastCyclicNetwork._A + "," + FastCyclicNetwork._B + "," + FastCyclicNetwork._C + "," + FastCyclicNetwork._D + ",");
                resultFile.Flush();
                break;
            }
        }



    }

    // Use this for initialization
	void PrepareExperiment ()
	{
	    Time.fixedDeltaTime = initialTimestep*Mathf.Pow(10f, speedModifier);
        mazesParent = new GameObject("Mazes Parent");
	    mazesParent.transform.parent = transform;
	    mazesFinished = 0;
	    totalMazes = widthCount*heightCount;
	    i = 0;
	    j = 0;
//	    rewardRight = rand.Next(2) == 0;
//	    switchIndex = rand.Next(10, 11);
	}

    public UnitController CreateEntity()
    {
        GameObject mazeObject = ((GameObject)Instantiate(tmaze, new Vector3(i * tmazeWidth - (widthCount - 1) * tmazeWidth / 2, j * tmazeHeight - (heightCount - 0.7f) * tmazeHeight / 2, 0), Quaternion.identity));
        mazeObject.transform.parent = mazesParent.transform;
        mazeObject.GetComponent<IterativeExperimentController>().Setup();
        mazeObject.GetComponent<IterativeExperimentController>().FixedUpdate();
        j++;
        if (j == heightCount)
        {
            j = 0;
            i++;
        }
        return mazeObject.GetComponent<MouseNeatUnit>();
    }
	
	// Update is called once per frame
	void Update () {
	    if (startCountdown > 0)
	    {
	        startCountdown--;
	        if (startCountdown == 0)
	        {
	            StartInteractive();
	        }
	    }

	}

    void LateUpdate()
    {
        if (SimulationFinished() && running && Time.time > startedTime+0.3f)
        {
            running = false;
            runsLeft--;
            if (runsLeft > 0)
            {
                finisherRoundNr++;

            }
        }

    }

    public void Finish()
    {
        mazesFinished++;
        if (mazesFinished == totalMazes)
        {
            foreach (IterativeExperimentController tmaze in GetComponentsInChildren<IterativeExperimentController>())
            {
                GameObject selectMouse = tmaze.transform.FindChild("SelectMouse").gameObject;
                selectMouse.SetActive(true);

            }
            foreach (IterativeExperimentController controller in GetComponentsInChildren<IterativeExperimentController>())
            {
                resultFile.Write(controller.Reward+",");
            }
            resultFile.WriteLine();
            resultFile.Flush();
        }

    }

    public bool Restart()
    {
        if (running)
        {
            running = true;
            gameObject.transform.DetachChildren();
            DestroyImmediate(mazesParent);
            PrepareExperiment();
            return true;
        } else
        {
            return false;
        }
    }

    void OnGUI()
    {
        

        GUI.Label(new Rect(40, 15, 100, 30), "Speed ");
        speedModifier = GUI.HorizontalSlider(new Rect(100, 20, 100, 30), speedModifier, 1f, -1f);
        if (!running)
        {

            if (!interactive || GUI.Button(new Rect(232, 5, 100, 35), "Start"))
            {

                if (startCountdown == 0)
                {
                    startCountdown = 6;
                }
            }

            
        }

        interactive = GUI.Toggle(new Rect(0, 0, 150, 20), interactive, "Interactive");
        

    }




    private void StartInteractive()
    {
        running = true;
        startedTime = Time.time;
        runsLeft = 1;
        if (firstRun)
        {
            firstRun = false;
            PrepareExperiment();
            GetComponent<Optimizer>().StartEA();
        }
        else
        {
            foreach (MouseNeatUnit neatUnit in GetComponentsInChildren<MouseNeatUnit>())
            {
                neatUnit.selected = neatUnit.GetComponentInChildren<SelectMouse>().selected;
            }
            finisherRoundNr++;
        }
    }

    private void StartAutomatic()
    {
        interactive = false;
        running = true;
        runsLeft = 10;
        if (firstRun)
        {
            firstRun = false;
            GetComponent<Optimizer>().StartEA();
        }
    }


    public bool SimulationFinished()
    {
        bool finished = true;
        foreach (IterativeExperimentController iterativeExperiment in GetComponentsInChildren<IterativeExperimentController>())
        {
            if (iterativeExperiment.HasNextExperiment() || iterativeExperiment.GetComponentInChildren<MouseController>() != null
          )
            {
                finished = false;
            }
        }
        return finished;
    }

    public bool RoundFinished(uint generation)
    {
        return finisherRoundNr >= generation;
    }
}
