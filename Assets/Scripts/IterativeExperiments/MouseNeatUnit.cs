﻿using SharpNeat.Phenomes;
using UnityEngine;
using System.Collections;

public class MouseNeatUnit : UnitController {

    public IBlackBox Box {get; private set;}
    public float Fintess;
    public bool selected;

    public override void Activate(IBlackBox box)
    {
        box.ResetState();
        Box = box;
    }

    public override void Stop()
    {
    }

    public override float GetFitness()
    {
        return selected ? 40f : Fintess;
    }
}
