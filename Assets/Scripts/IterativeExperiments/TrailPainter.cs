﻿using UnityEngine;
using System.Collections;

public class TrailPainter : MonoBehaviour
{

    private int lineIndex = 0;
	// Use this for initialization
	void Start () {
	
	}

    void FixedUpdate()
    {
        LineRenderer line = GetComponentInChildren<LineRenderer>();
        line.SetVertexCount(lineIndex+1);
        line.SetPosition(lineIndex, transform.position);
        lineIndex++;
    }
}
