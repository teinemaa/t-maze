﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using System.Collections;

public class IterativeExperimentController : ExperimentController {

    private int populationSize = 1;
    private float mouseSpeed = 1f;
	private float timeLimitSeconds = 100f;
	private float smallReward = 0.2f;
	private float bigReward = 1f;
    private List<MouseController> mouseControllers = new List<MouseController>();
    private GameObject trails;
    private float top = 0f;
    private float left = 0f;
    private float right = 0f;

    private int itereationsLeft;
    private int iterations;
    private bool rewardRight;
    private int switchIndex;
    private Hud hud1;
    private Hud hud2;
    private Hud active;

    void Start()
    {
        trails = new GameObject("Trails");
        trails.transform.parent = transform;
        Reward = 0;
    }

    public override void Init()
    {
        mouseSpeed = RandomContinuousController.stepDistance / Time.fixedDeltaTime;
        timeLimitSeconds = 9999999999f;
        itereationsLeft = 2*iterations;
        hud1 = transform.FindChild("Hud1").GetComponentInChildren<Hud>();
        hud2 = transform.FindChild("Hud2").GetComponentInChildren<Hud>();
        hud1.Init(iterations);
        hud2.Init(iterations);
        active = hud1;
    }

    public override bool HasNextExperiment()
    {
        if (itereationsLeft > 0)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public override Experiment RunNextExperiment()
    {
        if (itereationsLeft == iterations)
        {
            GetComponent<MouseNeatUnit>().Box.ResetState();
            active = hud2;
        }
        itereationsLeft--;
        for (int i = 0; i < populationSize; i++)
        {
			GameObject mouseControllerObject = ((GameObject)Instantiate(mouseGameObject, transform.position+Vector3.back, Quaternion.identity));
            var mouseController = mouseControllerObject.AddComponent<RandomContinuousController>();
			mouseController.transform.parent = transform;
            mouseControllers.Add(mouseController);
			mouseController.GetComponent<MouseProperties>().Speed = mouseSpeed;
        }
        bool bigRewardLeft = !rewardRight;
        if (switchIndex-1 == itereationsLeft%iterations)
        {

            active.AddSwitch();
        }
        if (itereationsLeft >= switchIndex && itereationsLeft < switchIndex + iterations)
        {
            bigRewardLeft = !bigRewardLeft;
        }
		float leftReward = bigRewardLeft ? bigReward : smallReward;
		float rightReward = bigRewardLeft ? smallReward : bigReward;
		return new Experiment(mouseControllers, leftReward, rightReward, timeLimitSeconds);
    }

    public override void EndExperiment()
    {
        foreach (MouseController mouseController in mouseControllers)
        {
            Vector3 position = mouseController.transform.position;
            top = Mathf.Min(Mathf.Max(position.y - transform.position.y, top), 4);
            left = Mathf.Max(position.x - transform.position.x, left);
            right = Mathf.Max(transform.position.x - position.x, right);
            Transform trail = mouseController.gameObject.transform.FindChild("Trail");
            mouseController.gameObject.transform.DetachChildren();
            trail.parent = trails.transform;
            GameObject death = ((GameObject)Instantiate(GetComponentInParent<InteractiveExperiments>().death, mouseController.transform.position, Quaternion.identity));
            death.transform.parent = trails.transform;
            active.AddReward(mouseController.LastReward);
            GetComponent<MouseNeatUnit>().Fintess += mouseController.LastReward;
            Reward += mouseController.LastReward;
            
            mouseController.gameObject.SetActive(false);
         
        }
        mouseControllers.Clear();
        if (!HasNextExperiment())
        {
            GetComponentInParent<InteractiveExperiments>().Finish();
            if (!GetComponentInParent<InteractiveExperiments>().interactive)
            {
                GetComponent<MouseNeatUnit>().Fintess += top;
                GetComponent<MouseNeatUnit>().Fintess += Mathf.Min(20 * left, 20 * right);
            }
        }
    }

    public float Reward { get; set; }


    public void Setup(bool rewardRight, int switchIndex, int iterations)
    {
        this.iterations = iterations;
        this.rewardRight = rewardRight;
        this.switchIndex = switchIndex;
    }

    public void Setup()
    {
        this.iterations = 20;
        this.rewardRight = true;
        this.switchIndex = 10;
    }
}
