﻿using UnityEngine;
using System.Collections;

public class RewardProperties : MonoBehaviour
{
    public Sprite bigReward;
    public Sprite smallReward;
    private float reward = 0;

    public float Reward
    {
        get { return reward; }
        set
        {
            reward = value;
            GetComponent<SpriteRenderer>().sprite = (reward == 1 ? bigReward : smallReward);
        }
    }
}
