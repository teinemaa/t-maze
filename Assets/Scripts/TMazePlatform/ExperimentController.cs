﻿using UnityEngine;
using System.Collections;

public abstract class ExperimentController : MonoBehaviour
{
    public GameObject mouseGameObject;

    public RewardProperties leftReward;
    public RewardProperties rightReward;

    private Experiment currentExperiment = null;
    private float timeLeft;

    private bool initialized = false;

	// Use this for initialization
    void Awake()
    {
	}
	
	// Update is called once per frame
	public void FixedUpdate () {
	    if (!initialized)
	    {
            Init();
	        initialized = true;
	    }
	    if (currentExperiment == null && HasNextExperiment())
	    {
            currentExperiment = RunNextExperiment();
            leftReward.Reward = currentExperiment.LeftReward;
            rightReward.Reward = currentExperiment.RightReward;
	        timeLeft = currentExperiment.TimeLimit;
	    }
	    if (currentExperiment != null)
	    {
	        bool anyMouseRemaining = false;
	        foreach (MouseController mouseController in currentExperiment.MouseControllers)
	        {
	            if (mouseController.gameObject.activeSelf)
	            {
	                anyMouseRemaining = true;
	            }
	        }
	        timeLeft -= Time.fixedDeltaTime;
            if (timeLeft < 0 || !anyMouseRemaining)
	        {
                EndExperiment();
	            currentExperiment = null;
	        }
	    }
	}

    public abstract void Init();
    
    public abstract bool HasNextExperiment();

    public abstract Experiment RunNextExperiment();
    public abstract void EndExperiment();

}





