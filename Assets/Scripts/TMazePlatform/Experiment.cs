﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Experiment
{
    public IEnumerable<MouseController> MouseControllers { get; private set; }
    public float LeftReward { get; private set; }
    public float RightReward { get; private set; }
    public float TimeLimit { get; private set; }

    public Experiment(IEnumerable<MouseController> mouseControllers, float leftReward, float rightReward, float timeLimit)
    {
        MouseControllers = mouseControllers;
        LeftReward = leftReward;
        RightReward = rightReward;
        TimeLimit = timeLimit;
    }
}



