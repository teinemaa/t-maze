﻿using UnityEngine;
using System.Collections;

public abstract class DiscreteMouseController : ContinuousMouseController
{
    public abstract DiscreteOutput AdvanceDiscrete(DiscreteInput input);

    protected DiscreteMouseController() : base(1f)
    {
    }


    public override ContinuousOutput AdvanceContinuous(ContinuousInput input)
    {
        int outputAngle = (int) AdvanceDiscrete(input);
        ContinuousOutput toReturn = new ContinuousOutput(outputAngle);

        return toReturn;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "wall")
        {
            gameObject.SetActive(false);
        }
    }
}

public interface DiscreteInput : SimpleInput
{
    bool IsTurn();
    bool IsHome();

}

public enum DiscreteOutput
{
    LEFT = -90,
    STRAIGHT = 0,
    RIGHT = 90
}



