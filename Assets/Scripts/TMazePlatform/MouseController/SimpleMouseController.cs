﻿using UnityEngine;
using System.Collections;

public abstract class SimpleMouseController : DiscreteMouseController
{
    public abstract SimpleOutput AdvanceSimple(SimpleInput input);

    public override DiscreteOutput AdvanceDiscrete(DiscreteInput input)
    {
        DiscreteOutput toReturn;
        if (input.IsTurn() || input.IsMazeEnd())
        {
            toReturn = (DiscreteOutput) AdvanceSimple(input);
        }
        else
        {
            toReturn = DiscreteOutput.STRAIGHT;
        }
        return toReturn;
    }
}

public interface SimpleInput
{
    float GetReward();
    bool IsMazeEnd();
}

public enum SimpleOutput
{
    LEFT = -90,
    RIGHT = 90
}



