﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class MouseProperties : MonoBehaviour
{
	public float Speed { get; set; }

	void Awake() {
		Speed = 1;
	}

}

