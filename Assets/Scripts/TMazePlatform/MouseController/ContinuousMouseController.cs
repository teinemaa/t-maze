﻿using UnityEngine;
using System.Collections;

public abstract class ContinuousMouseController : MouseController
{
    public abstract ContinuousOutput AdvanceContinuous(ContinuousInput input);

    private readonly float moveDistance;

    protected ContinuousMouseController(float moveDistance)
    {
        this.moveDistance = moveDistance;
    }

    public override MouseOutput AdvanceMouse(MouseInput input)
    {
        ContinuousOutput continuousOutput = AdvanceContinuous(input);
        return new MouseOutput(continuousOutput.TurnAngle, moveDistance);
    }
}

public interface ContinuousInput : DiscreteInput
{
    float GetRangeFinderDistance(float angle);

}

public class ContinuousOutput
{
    public float TurnAngle { get; private set; }

    public ContinuousOutput(float turnAngle)
    {
        TurnAngle = turnAngle;
    }

}
