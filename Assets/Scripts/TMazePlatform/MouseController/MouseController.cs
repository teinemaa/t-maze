﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(MouseProperties))]
public abstract class MouseController : MonoBehaviour
{
    private int raycastLayerMask;
    public abstract MouseOutput AdvanceMouse(MouseInput input);

    private int updatesToNextMove = 1;
    private Color orange = new Color(1, 0.5f, 0);

    private HashSet<GameObject> collidingObjects = new HashSet<GameObject>();
    private Vector3 startPosition;

    public float LastReward { get; private set; }

    void Awake()
    {
        raycastLayerMask = LayerMask.GetMask(new string[] { "walls" });
        startPosition = transform.position;
    }

    void FixedUpdate()
    {
        if (updatesToNextMove <= 0)
        {
            
            LastReward = GetReward();
            if (LastReward == 0)
            {
                float distanceFromStart = Vector3.Distance(startPosition, transform.position);
                //LastReward = distanceFromStart/100;
            }
            MouseOutput mouseOutput = AdvanceMouse(new MouseInput(GetRangeFinderDistance, GetReward, IsHome, IsTurn, IsMazeEnd));
            rigidbody2D.rotation -= mouseOutput.TurnAngle;
            float mouseSpeed = GetComponent<MouseProperties>().Speed;
            rigidbody2D.velocity = mouseSpeed * Vector2.up.Rotate(rigidbody2D.rotation);
            float timeToMoveStraight = mouseOutput.MoveDistance / mouseSpeed;
            updatesToNextMove = Mathf.RoundToInt(timeToMoveStraight / Time.fixedDeltaTime);
            if (IsMazeEnd())
            {
                gameObject.SetActive(false);
            }
            collidingObjects.Clear();
        }
        updatesToNextMove--;

    }


    void OnTriggerEnter2D(Collider2D coll)
    {
        collidingObjects.Add(coll.gameObject);
    }
    void OnTriggerStay2D(Collider2D coll)
    {
        collidingObjects.Add(coll.gameObject);
    }

    private float GetRangeFinderDistance(float angle)
    {
        Vector2 directionVector = Vector2.up.Rotate(rigidbody2D.rotation - angle);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, directionVector, 10f, raycastLayerMask);
        Vector2 ray = (Vector2)transform.position - hit.point;
        Debug.DrawLine(transform.position, hit.point, orange);
        return ray.magnitude - GetComponent<CircleCollider2D>().radius;
    }

    private float GetReward()
    {
        GameObject rewardProperties = GetCollidingObjectByTag("reward");
        if (rewardProperties == null)
        {
            return 0;
        }
        else
        {
            return rewardProperties.GetComponent<RewardProperties>().Reward;
        }
    }
    private bool IsMazeEnd()
    {
        return GetCollidingObjectByTag("reward") != null;
    }

    private bool IsTurn()
    {
        return GetCollidingObjectByTag("turn") != null;
    }

    private bool IsHome()
    {
        return GetCollidingObjectByTag("home") != null;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        gameObject.SetActive(false);
    }


    private GameObject GetCollidingObjectByTag(string tag)
    {
        foreach (GameObject collidingObject in collidingObjects)
        {
            if (collidingObject.tag == tag)
            {
                Vector3 dif = collidingObject.transform.position - transform.position;
                if (Math.Abs(dif.x) < 0.5f && Mathf.Abs(dif.y) < 0.5f)
                {
                    return collidingObject;
                }
            }
        }
        return null;
    }


}

public class MouseInput : ContinuousInput
{
    private readonly Func<float, float> rangeFinderDistance;
    private readonly Func<float> reward;
    private readonly Func<bool> home;
    private readonly Func<bool> turn;
    private readonly Func<bool> mazeEnd;

    public MouseInput(Func<float, float> rangeFinderDistance, Func<float> reward, Func<bool> home, Func<bool> turn, Func<bool> mazeEnd)
    {
        this.rangeFinderDistance = rangeFinderDistance;
        this.reward = reward;
        this.home = home;
        this.turn = turn;
        this.mazeEnd = mazeEnd;
    }

    public float GetRangeFinderDistance(float angle)
    {
        return rangeFinderDistance(angle);
    }

    public float GetReward()
    {
        return reward();
    }

    public bool IsHome()
    {
        return home();
    }
    public bool IsTurn()
    {
        return turn();
    }

    public bool IsMazeEnd()
    {
        return mazeEnd();
    }



}

public class MouseOutput
{
    public float TurnAngle { get; private set; }
    public float MoveDistance { get; private set; }

    public MouseOutput(float turnAngle, float moveDistance)
    {
        TurnAngle = turnAngle;
        MoveDistance = moveDistance;
    }
}

