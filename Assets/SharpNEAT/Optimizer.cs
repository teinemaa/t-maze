﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using SharpNeat.Core;
using SharpNeat.EvolutionAlgorithms;
using SharpNeat.Genomes.Neat;
using SharpNeat.Phenomes;
using UnityEngine;


public class Optimizer : MonoBehaviour
{
    private const int NUM_INPUTS = 4;
    private const int NUM_OUTPUTS = 1;
    private static NeatEvolutionAlgorithm<NeatGenome> _ea;

    //public float TrialDuration;
    public float StoppingFitness;
    public int Trials;
    private readonly Dictionary<IBlackBox, UnitController> ControllerMap = new Dictionary<IBlackBox, UnitController>();
    private bool EARunning;
    private double Fitness;
    private uint Generation;
    private float accum;
    private string champFileSavePath;

    private SimpleExperiment experiment;
    private int frames;
    private string popFileSavePath;
    private DateTime startTime;
    private float timeLeft;
    private float updateInterval = 12;

    public void StartEA()
    {
        Utility.DebugLog = true;
        Utility.Log("Starting PhotoTaxis experiment");
        // print("Loading: " + popFileLoadPath);
        _ea = experiment.CreateEvolutionAlgorithm(popFileSavePath);
        startTime = DateTime.Now;

        _ea.UpdateEvent += ea_UpdateEvent;
//        _ea.PausedEvent += new EventHandler(ea_PauseEvent);

        int evoSpeed = 25;

        //   Time.fixedDeltaTime = 0.045f;
        //Time.timeScale = evoSpeed;       
        _ea.StartContinue();
        EARunning = true;
    }

    public void StopEA()
    {
        if (_ea != null && _ea.RunState == RunState.Running)
        {
            _ea.Stop();
        }
    }

    public void Evaluate(IBlackBox box)
    {
        UnitController controller = GetComponent<InteractiveExperiments>().CreateEntity();
        ControllerMap.Add(box, controller);

        controller.Activate(box);
    }

    public void StopEvaluation(IBlackBox box)
    {
    }

    /*
    public void RunBest()
    {
        Time.timeScale = 1;

        NeatGenome genome = null;


        // Try to load the genome from the XML document.
        try
        {
            using (XmlReader xr = XmlReader.Create(champFileSavePath))
                genome = NeatGenomeXmlIO.ReadCompleteGenomeList(xr, false, (NeatGenomeFactory)experiment.CreateGenomeFactory())[0];


        }
        catch (Exception e1)
        {
            // print(champFileLoadPath + " Error loading genome from file!\nLoading aborted.\n"
            //						  + e1.Message + "\nJoe: " + champFileLoadPath);
            return;
        }

        // Get a genome decoder that can convert genomes to phenomes.
        var genomeDecoder = experiment.CreateGenomeDecoder();

        // Decode the genome into a phenome (neural network).
        var phenome = genomeDecoder.Decode(genome);

        GameObject obj = Instantiate(Unit, Unit.transform.position, Unit.transform.rotation) as GameObject;
        UnitController controller = obj.GetComponent<UnitController>();

        ControllerMap.Add(phenome, controller);

        controller.Activate(phenome);
    }*/

    public float GetFitness(IBlackBox box)
    {
        if (ControllerMap.ContainsKey(box))
        {
            float fitness = ControllerMap[box].GetFitness();

            ControllerMap.Remove(box);

            if (ControllerMap.Count == 0)
            {
                if (GetComponent<InteractiveExperiments>().Restart())
                {
                    writeNetwork();
                }
            }
            return fitness;
        }
        return 0;
    }

    public bool RoundFinished()
    {
        return GetComponent<InteractiveExperiments>().RoundFinished(Generation);
    }

    private void Start()
    {
        Utility.DebugLog = true;
        experiment = new SimpleExperiment();
        XmlDocument xmlConfig = new XmlDocument();
        TextAsset textAsset = (TextAsset) Resources.Load("experiment.config");
        xmlConfig.LoadXml(textAsset.text);
        experiment.SetOptimizer(this);

        experiment.Initialize("Car Experiment", xmlConfig.DocumentElement, NUM_INPUTS, NUM_OUTPUTS);

        champFileSavePath = Application.persistentDataPath + string.Format("/{0}.champ.xml", "car");
        popFileSavePath = Application.persistentDataPath + string.Format("/{0}.pop.xml", "car");

        print(champFileSavePath);
    }

    // Update is called once per frame
    private void Update()
    {
        //  evaluationStartTime += Time.deltaTime;

        timeLeft -= Time.deltaTime;
        accum += Time.timeScale/Time.deltaTime;
        ++frames;

        if (timeLeft <= 0.0)
        {
            float fps = accum/frames;
            timeLeft = updateInterval;
            accum = 0.0f;
            frames = 0;
            //   print("FPS: " + fps);
            if (fps < 10)
            {
                //Time.timeScale = Time.timeScale - 1;
                print("Lowering time scale to " + Time.timeScale);
            }
        }
    }

    private void ea_UpdateEvent(object sender, EventArgs e)
    {
        Utility.Log(string.Format("gen={0:N0} bestFitness={1:N6}",
            _ea.CurrentGeneration, _ea.Statistics._maxFitness));

        Fitness = _ea.Statistics._maxFitness;
        Generation = _ea.CurrentGeneration;


        //    Utility.Log(string.Format("Moving average: {0}, N: {1}", _ea.Statistics._bestFitnessMA.Mean, _ea.Statistics._bestFitnessMA.Length));
    }

    private void writeNetwork()
    {
        //Time.timeScale = 1;
        try
        {
            XmlWriterSettings _xwSettings = new XmlWriterSettings();
            _xwSettings.Indent = true;
            // Save genomes to xml file.        
            DirectoryInfo dirInf = new DirectoryInfo(Application.persistentDataPath);
            if (!dirInf.Exists)
            {
                Debug.Log("Creating subdirectory");
                dirInf.Create();
            }
            using (XmlWriter xw = XmlWriter.Create(popFileSavePath, _xwSettings))
            {
                experiment.SavePopulation(xw, _ea.GenomeList);
            }
            // Also save the best genome

            using (XmlWriter xw = XmlWriter.Create(champFileSavePath, _xwSettings))
            {
                experiment.SavePopulation(xw, new[] {_ea.CurrentChampGenome});
            }
            DateTime endTime = DateTime.Now;
            Utility.Log("Total time elapsed: " + (endTime - startTime));

            StreamReader stream = new StreamReader(popFileSavePath);
        }
        catch (SystemException e)
        {
        }

        //EARunning = false;        
    }

    private void OnGUI()
    {
        /*

        if (GUI.Button(new Rect(10, 110, 100, 40), "Run best"))
        {
            //RunBest();
        }*/

        GUI.Button(new Rect(10, Screen.height - 70, 100, 60),
            string.Format("Generation: {0}\nFitness: {1:0.00}", Generation, Fitness));
    }
}